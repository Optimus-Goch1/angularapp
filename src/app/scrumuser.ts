export class Scrumuser {
  constructor(
    public fullname: string,
    public email: string,
    public password: string,
    public usertype: string,
  ) {}
}

export class Scrumuserdata {
  constructor(
    public email: string,
    public password: string,
    public projectname: string
  ){}
}
