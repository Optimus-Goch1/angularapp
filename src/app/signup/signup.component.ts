import {Component, OnInit} from '@angular/core';
import {Scrumuser} from "../scrumuser";
import {ScrumdataService} from "../scrumdata.service";

@Component({
  selector: ' app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private _scrumdataService: ScrumdataService) {

  }

  ngOnInit() {
  }

  userTypes = ['Developer', 'Owner'];
  scrumUserModel = new Scrumuser('Johndoe', 'johndoe@linuxjobber.com', '', 'Developer');
  feedback = '';

  onSubmit() {
    console.log(this.scrumUserModel);
    this._scrumdataService.signup(this.scrumUserModel).subscribe(
      data => {
        console.log('Success!', data)
        this.feedback = 'Your account was successfully created'
      },
      error => {
        console.error('Error!', error)
        this.feedback = ' Sorry, signup was not successful'
      }
      )


  }
}
