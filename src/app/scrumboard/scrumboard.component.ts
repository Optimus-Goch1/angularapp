import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ScrumdataService} from "../scrumdata.service";
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';



@Component({
  selector: 'app-scrumboard',
  templateUrl: './scrumboard.component.html',
  styleUrls: ['./scrumboard.component.css']
})
export class ScrumboardComponent implements OnInit {

  tftw = [];
  tftd = [];
  verify = [];
  done = [];


  constructor(private _route : ActivatedRoute, private _scrumdataService : ScrumdataService) { }

  project_id = 0

  _participants =[]

  ngOnInit(){
    this.project_id = parseInt((this._route.snapshot.paramMap.get('project_id')));
    this.getProjectGoals();
  }


  drop(event: CdkDragDrop<string[]>){
    if (event.previousContainer === event.container){
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data, event.previousIndex, event.currentIndex);
    }
  }


  getProjectGoals(){
    this._scrumdataService.allProjectGoals(this.project_id).subscribe(
      data =>{
        console.log(data)
        this._participants = data['data']
      },
      error => {
        console.log('Error!', error)
      }
    )
  }
}
